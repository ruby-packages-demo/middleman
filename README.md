# middleman

[**middleman**](https://rubygems.org/gems/middleman)
![Gem download rank](https://img.shields.io/gem/rt/middleman)
Static site generator

* https://gitlab.com/pages/middleman
* Used by GitLab (2021).
